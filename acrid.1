.TH "ACRID" "1" "2024-01-08" "Geek Central" "PDF Hacks"
.
.SH NAME
acrid - examine/manipulate metadata in a PDF file.
.
.SH SYNOPSIS
.B acrid
.RI cmd " [" options "...]"
.IR file ...
.
.SH DESCRIPTION
.B acrid
is a command-line tool for examining and changing the contents of the
.B Info
dictionary in a PDF file. Entries can be added or removed, or
their associated values changed. There is also an option to delete
the XMP data.
.
.SH SUBCOMMANDS
The operation to perform depends on
.IR cmd ,
as do the valid associated
.IR options ,
as follows:
.
.TP
\fBhelp\fR [\fIcmd\fR]
with
.IR cmd ,
displays help for the specified command. If
.I cmd
is omitted, shows a list of valid commands.
.
.TP
\fBshowinfo\fR \fIin-pdf-file\fR
Shows the current info from the specified PDF file in JSON format, including a
dump of the XMP data, if present.
.
.TP
\fBgetinfo\fR [\fB\-\-default=\fIdefault_value\fR] \fIin-pdf-file\fR \fIkeyword\fR
Shows just the specified keyword entry from the Info dictionary. If the keyword
is not present, then the \fIdefault_value\fR is shown, if specified; if no
default is specified, an error is raised.
.
.TP
\fBgetxmp\fR [\fB\-\-default=\fIdefault_value\fR] \fIin-pdf-file\fR
Outputs just the XMP data from the file; if there is no XMP data, and
\fIdefault_value\fR is specified, that is output instead. Otherwise
an error is raised.
.
.TP
\fBhasinfo\fR \fIin-pdf-file\fR \fIkeyword\fR
Produces no output, but returns an exit status indicating if the
specified metadata value is present.
.
.TP
\fBhasxmp\fR \fIin-pdf-file\fR
Produces no output, but returns an exit status indicating if XMP data is present.
.
.TP
\fBsetinfo\fR [\fIoptions\fR...] \fIin-pdf-file\fR [\fIout-pdf-file\fR]
Reads
.IR in-pdf-file ,
makes changes to the metadata as specified by the options, and saves
the changed file as
.IR out-pdf-file .
The output filename must be specified unless \fB\-\-inplace\fR is present,
in which case the output filename is the same as the input filename.
Valid
.I options
are:
.
.RS 4
.
.TP
.B \-\-allow\-nonstd
Without this option, the only
.IR keyword s
that can be specified to the
.B \-\-set
and
.B \-\-del
options are
.BR Author ", " CreationDate ", " Creator ", " Keywords ", " ModDate ", " Producer ", " Subject ", " Title
and
.BR Trapped .
Specifying this option allows you to include your own nonstandard keywords as well.
.
.TP
.B \-\-delall
Deletes all keys in the
.B Info
dictionary. If specified together with
.BR \-\-set ,
then the deletions are done first, then the
.B \-\-set
options are performed to put back the specified keys.
.
.TP
.B \-\-delxmp
Deletes any XMP metadata, if this is present. The presence of XMP data is
supposed to override the
.B Info
dictionary and be a replacement for it, so if you are making changes to the
.BR Info ,
getting rid of the XMP data is probably a good idea.
.
.TP
.BI \-\-del= keyword
Deletes the
.B Info
entry with the specified
.IR keyword .
May be specified more than once to delete multiple
.IR keyword s
in a single operation.
.
.TP
.B \-\-inplace
Specifies that the output file is to replace the input file.
This is done in a safe fashion, so that the input remains
untouched if any error occurs.
This option requires the installation of the
.UR https://gitlab.com/ldo/python_linuxfs
linuxfs
.UE
module.
.
.TP
.BI \-\-set= keyword = value
Sets the
.B Info
entry with the specified
.I keyword
to the specified
.IR value .
Adds such an entry with that
.I keyword
if there isn’t one already, or replaces an existing
entry if there is one. May be specified more than once to set/add multiple
.IR keyword s
in a single operation.
.RE
.
.TP
\fBparse_date\fR \fIdate_time\fR
Parses a date-time string in the common \fBD:\fIYYYYmmddHHMMss[±tzd]\fR format
(as for example found as the values of the \fBCreationDate\fR or \fBModDate\fR fields)
returning a UTC seconds timestamp.
.
.TP
\fBformat_date \fIutc_seconds\fR
Formats a UTC seconds timestamp into the common PDF form.
Valid
.I options
are:
.
.RS 4
.TP
\fB\-\-timezone=\fItimezone\fR
specifies the local timezone for which to format the time.
.
.TP
\fB\-\-utc\fR
specifies that the time is to be formatted as UTC, with a “Z” suffix
in place of a timezone delta.
.
.PP
At most one of \fB\-\-timezone\fR or \fB\-\-utc\fR can be specified.
If neither is specified, the local timezone is used.
.RE
.
.SH NOTES
.
Requires the
.UR https://github.com/pikepdf/pikepdf
pikepdf
.UE
toolkit.
.
The \fBformat_date \-\-timezone=\fItimezone\fR feature only
works in Python 3.9 or later, as the
.UR https://docs.python.org/3/library/zoneinfo.html
zoneinfo
.UE
module is not available in earlier Python versions.
.
.SH BUGS
.
According to the latest PDF specs, XMP metadata is supposed to be a replacement
for the
.B Info
dictionary, which is now deprecated.
.
.SH EXAMPLES
.
.PP
.RS 4
acrid showinfo fred.pdf
.RE
.PP
Displays any
.B Info
and XMP data for the file
.BR fred.pdf .
.
.PP
.RS 4
.nf
acrid setinfo \-\-delxmp \-\-set=Author=\(dqFred Dagg\(dq \e
.RS 4
\-\-set=Creator=\(dqNot LibreOffice\(dq \-\-del=Producer \e
fred.pdf dagg.pdf
.RE
.fi
.RE
.PP
Copies
.B fred.pdf
to
.BR dagg.pdf ,
getting rid of any XMP data, setting the
.B Author
and
.B Creator
fields of the
.B Info
dictionary and deleting any
.B Producer
entry (if present) in the process.
